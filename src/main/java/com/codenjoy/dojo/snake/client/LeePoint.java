package com.codenjoy.dojo.snake.client;

import com.codenjoy.dojo.client.AbstractBoard;
import com.codenjoy.dojo.snake.model.Elements;

import java.util.Comparator;
import java.util.Objects;

public class LeePoint extends AbstractBoard<Elements>{
    private int x;
    private int y;

    public LeePoint(int x, int y) {
        this.x = x;
        this.y = y;
    }
    public int x() {
        return x;
    }

    public int y() {
        return y;
    }

    LeePoint move(LeePoint delta) {
        return new LeePoint(
                this.x + delta.x,
                this.y + delta.y);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LeePoint leePoint = (LeePoint) o;
        return x == leePoint.x && y == leePoint.y;
    }

    @Override
    public int hashCode() {
        return Objects.hash(x, y);
    }

    @Override
    public Elements valueOf(char c) {
        return null;
    }
    @Override
    public String toString() {
        return String.format("[%d,%d]", x, y);
    }
}
