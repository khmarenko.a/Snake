package com.codenjoy.dojo.snake.client;

/*-
 * #%L
 * Codenjoy - it's a dojo-like platform from developers to developers.
 * %%
 * Copyright (C) 2018 Codenjoy
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */


import com.codenjoy.dojo.client.Solver;
import com.codenjoy.dojo.client.WebSocketRunner;
import com.codenjoy.dojo.services.Dice;
import com.codenjoy.dojo.services.Direction;
import com.codenjoy.dojo.services.Point;
import com.codenjoy.dojo.services.RandomDice;

import java.util.*;
import java.util.stream.Stream;

/**
 * User: Oleksandr Khmarenko
 */
public class YourSolver implements Solver<Board> {
    private int cutLevel = 45;


    private Dice dice;

    private List<LeePoint> fullSnake = new ArrayList<>();

    private static List<Direction> movesList = new ArrayList<>();

    public YourSolver(Dice dice) {
        this.dice = dice;
    }

    private int rangeTo(Board board, LeePoint target) {
        return doSolve(board, target, true)
                .isPresent()
                ? doSolve(board, target, true).get().size()
                : 0;
    }

    private Optional<List<Direction>> solve(Board board, LeePoint target) {
        LeePoint stone = new LeePoint(board.getStones().get(0).getX(), board.getStones().get(0).getY());
        LeePoint apple = new LeePoint(board.getApples().get(0).getX(), board.getApples().get(0).getY());
        if (board.getSnake().size() >= cutLevel
                && rangeTo(board, apple) > rangeTo(board, stone)
                && traceIsPresent(board, stone, apple, -10)) {
            target = stone;
        }
        return doSolve(board, target, false);
    }

    private LeeBoard fillObstacles(Board board) {
        //int boardSize = board.getWalls().size() / 4 + 1;
        LeeBoard leeBoard = new LeeBoard(15, 15);

        List<Point> obstacles = board.getBarriers();
        obstacles.removeAll(board.getSnake());

        for (Point point : obstacles) {
            leeBoard.setObstacle(new LeePoint(point.getX(), point.getY()));
        }
        for (Point point : board.getSnake()) {
            leeBoard.setSnake(new LeePoint(point.getX(), point.getY()));
        }
        LeePoint tail = fullSnake.get(fullSnake.size() - 1);
        leeBoard.set(new LeePoint(tail.x(), tail.y()), 0);
        return leeBoard;
    }

    private List<Direction> fillDirections(List<LeePoint> path) {
        for (int i = 1; i < path.size(); i++) {
            movesList.add(Stream.of((new LeePoint(path.get(i).x() - path.get(i - 1).x(),
                            path.get(i).y() - path.get(i - 1).y())))
                    .map(s -> s.equals(new LeePoint(1, 0)) ? Direction.RIGHT :
                            s.equals(new LeePoint(-1, 0)) ? Direction.LEFT :
                                    s.equals(new LeePoint(0, 1)) ? Direction.UP :
                                            Direction.DOWN)
                    .findFirst().get()
            );
        }
        return movesList;
    }

    private void checkTargetIsStone(Board board, LeeBoard leeBoard, LeePoint leeTarget) {
        if (leeTarget.x() == board.getStones().get(0).getX()
                && leeTarget.y() == board.getStones().get(0).getY()) {
            leeBoard.set(new LeePoint(leeTarget.x(), leeTarget.y()), 0);
        }
    }

    private boolean traceIsPresent(Board board, LeePoint startPoint, LeePoint finishPoint, int addTail) {
        //int boardSize = board.getWalls().size() / 4 + 1;
        LeeBoard leeBoard = new LeeBoard(15, 15);

        List<Point> obstacles = board.getBarriers();
        obstacles.removeAll(board.getSnake());

        for (Point point : obstacles) {
            leeBoard.setObstacle(new LeePoint(point.getX(), point.getY()));
        }
        Optional<List<LeePoint>> snake = getTrace(board, startPoint);

        List<LeePoint> snakeTrace = new ArrayList<>();
        if (snake.isPresent()) snakeTrace = snake.get();

        List<LeePoint> snakePlusTrace = new ArrayList<>();

        List<LeePoint> reversSnake = new ArrayList<>(fullSnake);
        reversSnake.subList(1, reversSnake.size());
        Collections.reverse(reversSnake);
        snakePlusTrace.addAll(reversSnake);
        snakePlusTrace.addAll(snakeTrace);
        snakePlusTrace = snakePlusTrace.subList(snakePlusTrace.size() - reversSnake.size() - addTail, snakePlusTrace.size());///add -1

        for (LeePoint point : snakePlusTrace) {
            leeBoard.setSnake(new LeePoint(point.x(), point.y()));
        }
        leeBoard.set(new LeePoint(board.getStones().get(0).getX(),
                board.getStones().get(0).getY()), 0);

        Optional<List<LeePoint>> trace = leeBoard.trace(startPoint, finishPoint);
        if (trace.isPresent()) {
            System.out.println("TRACE TO " + finishPoint + " FROM " + startPoint + " IS PRESENT");
            return true;
        } else {
            System.out.println("NO TRACE TO " + finishPoint + " FROM " + startPoint);
            return false;
        }
    }

    private Optional<List<LeePoint>> getTrace(Board board, LeePoint leeTarget) {
        LeeBoard leeBoard = fillObstacles(board);
        checkTargetIsStone(board, leeBoard, leeTarget);
        LeePoint start = new LeePoint(board.getHead().getX(), board.getHead().getY());
        return leeBoard.trace(start, leeTarget);
    }

    public Optional<List<Direction>> doSolve(Board board, LeePoint leeTarget, boolean checkRangeTo) {

        movesList = new ArrayList<>();
        Optional<List<LeePoint>> result = getTrace(board, leeTarget);

        if (result.isPresent()) {
            movesList = fillDirections(result.get());
            if (!checkRangeTo) {
                movesList = movesList.subList(0, 1);
            }
            return Optional.of(movesList);
        } else return Optional.empty();
    }

    void snakeSizeChange(Board board) {
        fullSnake.add(0, new LeePoint(board.getHead().getX(), board.getHead().getY()));
        if (fullSnake.size() > board.getSnake().size()) fullSnake = fullSnake.subList(0, board.getSnake().size());
    }

    Boolean toApple(Board board, LeePoint apple, LeePoint stone) {
        if (solve(board, apple).isPresent()
                && traceIsPresent(board, apple, stone, 1)
        )
            return true;
        else return false;
    }

    Boolean toCorners(Board board, LeePoint stone) {
        if (solve(board, new LeePoint(1, 1)).isPresent()
                && traceIsPresent(board, new LeePoint(1, 1), stone, 1)) {
            System.out.println("GO 1,1 !!!");
            return true;
        } else if (solve(board, new LeePoint(13, 13)).isPresent()
                && traceIsPresent(board, new LeePoint(13, 13), stone, 1)) {
            System.out.println("GO 13,13 !!!");
            return true;
        } else if (solve(board, new LeePoint(1, 13)).isPresent()
                && traceIsPresent(board, new LeePoint(1, 13), stone, 1)) {
            System.out.println("GO 1,13 !!!");
            return true;
        } else if (solve(board, new LeePoint(13, 1)).isPresent()
                && traceIsPresent(board, new LeePoint(13, 1), stone, 1)) {
            System.out.println("GO 13,1 !!!");
            return true;
        } else return false;
    }

    Boolean toStoneOrToApple(Board board, LeePoint apple, LeePoint stone) {
        if (solve(board, stone).isPresent()) {
            System.out.println("GO TO STONE !!!");
            return true;
        } else if (solve(board, apple).isPresent()) {
            System.out.println("GO TO APPLE !!!");
            board.getHead().distance(board.getBarriers().get(0));
            return true;
        } else return false;
    }

    Boolean wastingTime(Board board) {
        if (solve(board, new LeePoint(board.getHead().getX(),
                (board.getHead().getY() + 1))).isPresent()) {
            System.out.println("UP!!!");
            return true;
        } else if (solve(board, new LeePoint(board.getHead().getX(),
                (board.getHead().getY() - 1))).isPresent()) {
            System.out.println("DOWN!!!");
            return true;
        } else if (solve(board, new LeePoint((board.getHead().getX() - 1),
                board.getHead().getY())).isPresent()) {
            System.out.println("LEFT!!!");
            return true;
        } else if (solve(board, new LeePoint((board.getHead().getX() + 1),
                board.getHead().getY())).isPresent()) {
            System.out.println("RIGHT!!!");
            return true;
        } else return false;
    }
    private String endOfRound(Board board){
        System.out.println("END OF ROUND!!!");
        if (board.getSnakeDirection().equals(Direction.UP)) return Direction.DOWN.toString();
        else if (board.getSnakeDirection().equals(Direction.DOWN)) return Direction.UP.toString();
        else if (board.getSnakeDirection().equals(Direction.LEFT)) return Direction.RIGHT.toString();
        else return Direction.LEFT.toString();
    }

    @Override
    public String get(Board board) {

        if (movesList.isEmpty()) {
            LeePoint apple = new LeePoint(board.getApples().get(0).getX(), board.getApples().get(0).getY());
            LeePoint stone = new LeePoint(board.getStones().get(0).getX(), board.getStones().get(0).getY());

            snakeSizeChange(board);

            if (toApple(board, apple, stone)) {
                return movesList.remove(0).toString();
            }
            else if (toCorners(board, stone)) {
                return movesList.remove(0).toString();
            }
            else if (toStoneOrToApple(board, apple, stone)) {
                return movesList.remove(0).toString();
            }
            else if (wastingTime(board)) {
                return movesList.remove(0).toString();
            }
            else {
                return endOfRound(board);
            }
        }
        return movesList.remove(0).toString();
    }

    public static void main(String[] args) {
        WebSocketRunner.runClient(
                // paste here board page url from browser after registration
                "http://206.81.21.158/codenjoy-contest/board/player/cep8dh7q6ejjbu2sh9m0?code=1833905707871201646",
                new YourSolver(new RandomDice()),
                new Board());
    }
}