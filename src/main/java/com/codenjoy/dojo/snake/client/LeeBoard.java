package com.codenjoy.dojo.snake.client;

import com.codenjoy.dojo.client.AbstractBoard;
import com.codenjoy.dojo.snake.model.Elements;

import java.util.*;
import java.util.stream.Collectors;

public class LeeBoard extends AbstractBoard<Elements> {
    private final List<LeePoint> deltas = new ArrayList<LeePoint>() {{
        add(new LeePoint(0, -1));
        add(new LeePoint(-1, 0));
        add(new LeePoint(1, 0));
        add(new LeePoint(0, 1));
    }};

    private final static int OBSTACLE = 99;
    private final static int SNAKE = 88;

    private final static int START = -1;

    private final int dimX;

    private final int dimY;
    private int[][] data;

    public LeeBoard(int dimX, int dimY) {
        this.dimX = dimX;
        this.dimY = dimY;
        this.data = new int[dimY][dimX];
    }

    void leePrintMe() {
        for (int row = 0; row < dimY; row++) {
            for (int col = 0; col < dimX; col++) {
                if (data[row][col] > 0 && data[row][col] < 88)
                    System.out.printf("%3d", data[row][col]);
                else if (data[row][col] == 88)
                    System.out.print("\u001B[31m" + " SS" + "\u001B[0m");
                else if (data[row][col] == 99)
                    System.out.print("\u001B[31m" + " XX" + "\u001B[0m");
                else System.out.print("  .");
            }
            System.out.println();
        }
        System.out.println();
    }

    int get(LeePoint p) {
        return this.data[p.y()][p.x()];
    }

    void set(LeePoint p, int val) {
        this.data[p.y()][p.x()] = val;
    }

    public boolean isOnBoard(LeePoint p) {
        return p.x() > 0 && p.x() < dimX - 1 && p.y() > 0 && p.y() < dimY - 1;
    }

    boolean isUnvisited(LeePoint p) {
        return get(p) == 0;
    }

    List<LeePoint> neighbors(LeePoint point) {
        return deltas.stream()
                .map(s -> s.move(point))
                .filter(this::isOnBoard)
                .collect(Collectors.toList());
    }

    List<LeePoint> neighborsUnvisited(LeePoint point) {
        return neighbors(point).stream()
                .filter(this::isUnvisited)
                .collect(Collectors.toList());
    }

    LeePoint neighborByValue(LeePoint point, int value) {
        return neighbors(point).stream()
                .filter(p -> get(p) == value)
                .filter(i -> get(new LeePoint(i.x() + 1, i.y())) == 88
                        || get(new LeePoint(i.x() - 1, i.y())) == 88
                        || get(new LeePoint(i.x(), i.y() + 1)) == 88
                        || get(new LeePoint(i.x(), i.y() - 1)) == 88
                        || get(new LeePoint(i.x() + 1, i.y())) == 99
                        || get(new LeePoint(i.x() - 1, i.y())) == 99
                        || get(new LeePoint(i.x(), i.y() + 1)) == 99
                        || get(new LeePoint(i.x(), i.y() - 1)) == 99
                        || get(new LeePoint(i.x(), i.y())) == value)
                .findFirst()
                .get();
    }

    Optional<List<LeePoint>> trace(LeePoint start, LeePoint finish) {
        boolean found = false;
        set(start, -1);
        Set<LeePoint> curr = new HashSet<>();
        int[] counter = new int[1];
        curr.add(start);
        while (!curr.isEmpty() && !found) {
            counter[0]++;

            List<LeePoint> next = curr.stream()
                    .map(this::neighborsUnvisited)
                    .flatMap(Collection::stream)
                    .collect(Collectors.toList());
            next.forEach(p -> set(p, counter[0]));
            if (next.contains(finish)) {
                found = true;
            }

            //leePrintMe();   ////////////////


            curr.clear();
            curr.addAll(next);
            next.clear();

        }
        if (found) {
            set(start, 0);
            ArrayList<LeePoint> path = new ArrayList<>();
            if (counter[0] == 1) {
                path.add(start);
                path.add(finish);
                return Optional.of(path);
            }
            path.add(finish);
            LeePoint curr_p = finish;
            while (counter[0] > 0) {
                counter[0]--;
                LeePoint prev_p = neighborByValue(curr_p, counter[0]);
                path.add(prev_p);
                curr_p = prev_p;
            }
            Collections.reverse(path);
            return Optional.of(path);
        } else return Optional.empty();
    }

    public void setObstacle(LeePoint point) {
        set(point, OBSTACLE);
    }

    public void setSnake(LeePoint point) {
        set(point, SNAKE);
    }

    @Override
    public Elements valueOf(char c) {
        return null;
    }
}
